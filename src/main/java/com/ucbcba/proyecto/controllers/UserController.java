package com.ucbcba.proyecto.controllers;



import com.ucbcba.proyecto.entities.User;
import com.ucbcba.proyecto.services.SecurityService;
import com.ucbcba.proyecto.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Set;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    //@Autowired
    //private UserValidator userValidator;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationInit(Model model) {
        model.addAttribute("user", new User());

        return "registration";//implementar html registration Carlos
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@Valid User user, BindingResult bindingResult, Model model) {
        ///userValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute(user);
            return "registration";
        }
        userService.save(user);
        securityService.autologin(user.getEmail(), user.getPasswordConfirm());
        Set<String> roles = AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
        if (roles.contains("ADMIN")) {
            return "redirect:/admin/home";
        }
        return "redirect:/user/home";}

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        Set<String> roles = AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
        if (roles.contains("ADMIN")) {
            return "redirect:/admin/home";
        }
        if (roles.contains("CLIENTE")) {
            return "redirect:/user/home";
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/admin/home", method = RequestMethod.GET)
    public String homeA(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        model.addAttribute("nameA",user.getUsername() + " " + user.getLastname());
        return "principalA";
    }

    @RequestMapping(value = "/user/home", method = RequestMethod.GET)
    public String homeU(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        model.addAttribute("nameA",user.getUsername() + " " + user.getLastname());
        return "principalU";
    }
    //@RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    @RequestMapping(value = {"/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        return "welcome";
    }
    @RequestMapping(value = {"/admin/"}, method = RequestMethod.GET)
    public String admin(Model model) {
        return "welcome";
    }
    @RequestMapping(value = {"/bienvenidos"}, method = RequestMethod.GET)
    public String welcome2(Model model) {
        Set<String> roles = AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
        if (roles.contains("ADMIN")) {
            return "redirect:/admin/home";
        }
        if (roles.contains("CLIENTE")) {
            return "redirect:/user/home";
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/admin/usuarios", method = RequestMethod.GET)
    public String listado(Model model) {
        model.addAttribute("usuarios", userService.listAllUsers());
        return "usuarios";
    }
}