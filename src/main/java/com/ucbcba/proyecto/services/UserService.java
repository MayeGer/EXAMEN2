package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.User;

/**
 * Created by amolina on 10/05/17.
 */
public interface UserService {
    void save(User user);
    User findUserByEmail(String email);

    User findByUsername(String username);

    Iterable<User> listAllUsers();
}