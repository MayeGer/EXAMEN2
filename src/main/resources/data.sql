INSERT INTO role (name)
SELECT * FROM (SELECT 'ADMIN') AS tmp
WHERE NOT EXISTS (
    SELECT name FROM role WHERE name = 'ADMIN'
) LIMIT 1;

INSERT INTO role (name)
SELECT * FROM (SELECT 'PROPIETARIO') AS tmp
WHERE NOT EXISTS (
    SELECT name FROM role WHERE name = 'PROPIETARIO'
) LIMIT 1;

INSERT INTO role (name)
SELECT * FROM (SELECT 'CLIENTE') AS tmp
WHERE NOT EXISTS (
    SELECT name FROM role WHERE name = 'CLIENTE'
) LIMIT 1;